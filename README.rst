docker-polarbytebot-gw2-devsubmissions-tracker
==============================================

* all actions to the database and all the actual logic is part of the `polarbytebot library <https://gitlab.com/networkjanitor/libpolarbytebot>`_
* this is a small microservice/dockercontainer which queues new submissions into the database
* these submissions are in this case links to other submissions made by arenanet employees in the /r/GuildWars2 subreddit