import logging
import prawcore.exceptions
import configparser
import os
import sys
from libpolarbytebot.tracker.guildwars2 import gen
from libpolarbytebot import polarbytebot

__version__ = '0.1.0'
__owner = 'Xyooz'
__source_link = 'https://gitlab.com/networkjanitor/docker-polarbytebot-gw2-devsubmissions-tracker'
__user_agent = f'polarbytebot-gw2-devsubmissions-tracker/{__version__} by /u/{__owner}'
__signature = f'\n' \
              f'\n' \
              f'---\n' \
              f'^(Beep boop. This message was created by a bot. Please message /u/{__owner} if you have any ' \
              f'questions, suggestions or concerns.) [^Source ^Code]({__source_link})'
__subreddits = 'guildwars2+test+gw2economy'
__microservice_id = 'gw2-devcm-track'

__enable_comment_gw2devtrack = True
__enable_comment_gw2devtrackalt = True


def run():
    path_to_conf = os.path.abspath(os.path.dirname(sys.argv[0]))
    path_to_conf = os.path.join(path_to_conf, 'settings.conf')
    cfg = configparser.ConfigParser()
    cfg.read(path_to_conf)
    pbb = polarbytebot.Polarbytebot(signature=__signature, microservice_id=__microservice_id,
                                    oauth_client_id=cfg.get('oauth2', 'client_id'),
                                    oauth_client_secret=cfg.get('oauth2', 'client_secret'),
                                    oauth_redirect_uri=cfg.get('oauth2', 'redirect_uri'),
                                    oauth_username=cfg.get('oauth2', 'username'), praw_useragent=__user_agent,
                                    oauth_refresh_token=cfg.get('oauth2', 'refresh_token'),
                                    database_system=cfg.get('database', 'system'),
                                    database_username=cfg.get('database', 'username'),
                                    database_password=cfg.get('database', 'password'),
                                    database_host=cfg.get('database', 'host'),
                                    database_dbname=cfg.get('database', 'database'))
    while True:
        safe_poll(pbb)


def safe_poll(pbb):
    try:
        for subm in pbb.reddit.subreddit(__subreddits).stream.submissions():
            name_list = pbb.guildwars2_list_developer()

            if __enable_comment_gw2devtrack:
                gw2devtrack(subm, name_list, pbb)

            if __enable_comment_gw2devtrackalt:
                gw2devtrackalt(subm, name_list, pbb)
    except prawcore.exceptions.RequestException as e:
        pass


def gw2devtrack(subm, name_list, pbb):
    if subm.author is not None and subm.author.name in name_list:
        infos = gen(subm.author.name, subm.title, subm.permalink, subm.selftext)
        pbb.create_submission('gw2devtrack', infos.title, infos.permalink, 'link')


def gw2devtrackalt(subm, name_list, pbb):
    if subm.author is not None and subm.author.name in name_list:
        infos = gen(subm.author.name, subm.title, subm.permalink, subm.selftext)
        pbb.create_submission('gw2devtrackalt', infos.title, infos.selfpost, 'self')


if __name__ == '__main__':
    run()
